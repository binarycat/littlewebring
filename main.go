// static site generator for small webrings
// allows hosting a webring on your site without special software
package main

import (
	"html/template"
	"os"
	"path/filepath"
	"net/url"
	"io"
	"strings"
	"fmt"
)

const helptext = `
littlemarkdown help
littlemarkdown generate OUTDIR < LINKS
littlemarkdown embed WEBRING_URL PAGE_URL
`

var redirectTempl = template.Must(template.New("redirect").Parse(`
<!DOCTYPE html>
<html>
<head><meta http-equiv="refresh" content="0;URL='{{.}}'" /></head>
<body>redirecting...</body>
</html>
`))

var indexTempl = template.Must(template.New("index").Parse(`
<!DOCTYPE html>
<html>
<head></head>
<body>
{{range . -}}
<a href="{{.}}">{{.}}</a><br/>
{{end -}}
</body>
</html>
`))

type EmbedOpt struct {
	Webring, Page string
}

var embedTempl = template.Must(template.New("embed").Parse(`
<div>
<h3>Webring</h3>
<a href="{{.Webring}}/{{.Page}}/prev.html">Previous</a>
<a href="{{.Webring}}/{{.Page}}/next.html">Next</a>
<br/>
<a href="{{.Webring}}/index.html">Index</a>
</div>
`))

func GenFile(path, url string) error {
	f, err := os.Create(path)
	if err != nil { return err }
	defer f.Close()
	return redirectTempl.Execute(f, url)
}

func GenWebring(dir string, links []string) error {
	mode := os.ModeDir | 0o777
	err := os.Mkdir(dir, mode)
	if err != nil { return err }
	l := len(links)
	for i, this := range links {
		prev := links[(i+l-1)%l]
		next := links[(i+l+1)%l]
		pageDir := filepath.Join(dir, url.QueryEscape(this))
		err = os.Mkdir(pageDir, mode)
		if err != nil { return err }
		err = GenFile(filepath.Join(pageDir, "prev.html"), prev)
		if err != nil { return err }
		err = GenFile(filepath.Join(pageDir, "next.html"), next)
		if err != nil { return err }
	}
	// generate index.html
	idxFile, err := os.Create(filepath.Join(dir, "index.html"))
	if err != nil { return err }
	defer idxFile.Close()
	return indexTempl.Execute(idxFile, links)
}

func ReadLines(r io.Reader) ([]string, error) {
	all, err := io.ReadAll(r)
	if err != nil { return nil, err } 
	parts := strings.Split(string(all), "\n")
	newParts := make([]string, 0, len(parts))
	for _, part := range parts {
		// remove any possible trailing carrage return or other whitespace
		newPart := strings.TrimSpace(part)
		// ignore empty lines and comments
		if len(newPart) == 0 || newPart[0] == '#' {
			continue
		}
		newParts = append(newParts, newPart)
	}
	return newParts, nil
}

func main() {
	if len(os.Args) == 1 {
		os.Args = append(os.Args, "help")
	}
	switch os.Args[1] {
	case "generate", "gen":
		links, err := ReadLines(os.Stdin)
		if err != nil { panic(err) }
		err = GenWebring(os.Args[2], links)
		if err != nil { panic(err) }
	case "embed":
		var eo EmbedOpt
		// strip trailing slash if present, we add one later
		eo.Webring = strings.TrimRight(os.Args[2], "/")
		// must be EXACTLY as provided in list of links
		eo.Page = url.QueryEscape(os.Args[3])
		err := embedTempl.Execute(os.Stdout, eo)
		if err != nil { panic(err) }
	default:
		fmt.Println("invalid command: ", os.Args[1])
		fallthrough
	case "help", "--help":
		fmt.Println(helptext)
	}

}
