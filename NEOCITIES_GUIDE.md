# a complete guide to hosting a webring on neocities

## step 1: download and install littlewebring
[install go](https://go.dev/doc/install) if it is not already present on your system, then run:

`go install git.envs.net/binarycat/littlewebring@latest`

## step 2: make the list of sites
find people to join your webring, and put their pages in a file `my_webring.txt`, one per line, like so:
```
https://first.example/
https://example.com/~userA/
https://example.com/~userB/
https://example.com/~userC/about.html
https://last.example/
```

save this list somewhere you can find it, you'll need it if you want to add sites to the webring later.

## step 3: generate webring folder
`littlewebring generate my_webring < my_webring.txt`

if the executable is not found, you can also run it through go:
`go run git.envs.net/binarycat/littlewebring@latest generate my_webring < my_webring.txt`

## step 4: upload to neocities
log into your neocities account (or create a new one), and go to <https://neocities.org/dashboard>.

drag and drop the my_webring folder into the dashbord page to upload it.

delete the local my_webring folder.

if you want to rename the folder to something else, do so now.

## step 5: have people add the links to their page
you will need to contact every person whose site you added to the webring, and have them add links to their page.

if your friend asked you to add myfriend.example to the webring, you can 
generate a snippet by running:
`littlewebring embed https://MYSITE.neocities.org/my_webring https://myfriend.example/'`
IMPORTANT: you must type you friend's page *exactly* as it appears in my_webring.txt, otherwise the links will not work properly.

this will output something like the following:
```
<div>
<h3>Webring</h3>
<a href="https://MYSITE.neocities.org/my_webring/https%3A%2F%2Fmyfriend.example%2F/prev.html">Previous</a>
<a href="https://MYSITE.neocities.org/my_webring/https%3A%2F%2Fmyfriend.example%2F/next.html">Next</a>
<br/>
<a href="https://MYSITE.neocities.org/my_webring/index.html">Index</a>
</div>
```

this can be reformatted however they like, as long as they have the links to prev.html and next.html

once everyone has added the links on their pages, the webring is officially up and running!

## adding/removing sites
if you ever want to change what sites are part of the webring:
A. add, subtract, or reorder sites in `my_webring.txt` as desired
B. delete the my_webring folder on neocities
D. repeat steps 3 and 4
E. for each **new** site, send them the links to add (described in step 5)
