<?php
$url = $_POST['url'];
$fp = fopen(__DIR__.'/applicants.txt', 'a')or die("Unable to open file!");
fwrite($fp, $url . "\n");
fclose($fp);
$url_coded = urlencode($url);
$webring_base = preg_replace("/[a-z.]+$", "", $_SERVER['REQUEST_URI'], 1);
$index_url = $webring_base.'/index.html';
$next_url = $webring_base.'/'.$url_coded.'/next.html';
$prev_url = $webring_base.'/'.$url_coded.'/prev.html';
$snippet = '<a href="'.$prev_url.'">←</a><a href="'.$index_url.'">index</a><a href="'.$next_url.'">→</a>';
?>
<!DOCTYPE html>
<html>
  <head>
	<meta charset="utf-8"/>
  </head>
  <body>
	<p>Your webring application has been submitted.  All pages must contain the following links <b>before</b> being submitted.</p>
	<textarea readonly><?php echo htmlentities($snippet)?></textarea>
  </body>
</html>
